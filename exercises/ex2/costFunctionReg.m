%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

function [J, grad] = costFunctionReg(theta, X, y, lambda)

    [J1, grad1] = costFunction(theta, X, y);

    thetaModified = [0 ; theta(2:end)];

    m = length(y);       % number of training examples
    J = J1 + (lambda / ( 2 * m ) ) * (thetaModified' * thetaModified);
    grad = grad1 + (lambda / m ) * thetaModified;

end

