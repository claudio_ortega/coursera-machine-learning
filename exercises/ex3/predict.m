%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

function p = predict(Theta1, Theta2, inX)

    m = size(inX, 1);

    % first stage: input: inX/a1, output a2
    a1 = inX;
    a1 = [ones(m,1) a1];
    a2 = sigmoid( a1 * Theta1' );

    % second stage: input
    a2 = [ones(m,1) a2];
    a3 = sigmoid( a2 * Theta2' );

    [~,p] = max(a3,[],2);
end
