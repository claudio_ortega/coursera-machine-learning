function a3 = computeTwoLayers(Theta1, Theta2, X)
    m = size(X, 1);

    a1 = [ones(m, 1), X];
    z2 = Theta1 * a1';
    g2 = sigmoid(z2);

    a2 = [ones(m, 1), g2'];
    z3 = Theta2 * a2';
    a3 = sigmoid(z3);
end

function p = predict2(Theta1, Theta2, X)
    tmp = computeTwoLayers(Theta1, Theta2, X);
    [~, p] = max(tmp, [], 2);
end
