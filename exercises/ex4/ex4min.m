
clear ; close all; clc

%% Setup the parameters you will use for this exercise
input_layer_size  = 3;
hidden_layer_size = 2;
num_labels = 4;

% sample #1 is [1,2,3]
X = [ [1,2,3] ; [4,5,6] ; [-1,-2,-3] ]
y = [ 1 ; 2 ; 3 ; 4 ]

% (2,3+1)
Theta1 = [ [1,2,3,4] ; [5,6,7,8] ]

% (4,2+1)
Theta2 = [ [1,2,3] ; [4,5,6] ; [7,8,9] ; [10,11,12] ]

nn_params = [Theta1(:) ; Theta2(:)]

[J,grad] = nnCostFunction(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, 0)
