%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices.
%
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

function [J, grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)

    % Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
    % for our 2 layer neural network
    Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                     hidden_layer_size, (input_layer_size + 1));

    Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                     num_labels, (hidden_layer_size + 1));

    out = computeTwoLayers(Theta1, Theta2, X);

    eye_mapping = eye(num_labels);
    % for the variable y, perform the mapping 1->[1,0,0,0]', 2->[0,1,0,0..]', .. 10->[0,0,..1]'
    % so then ehe_10(y,:) will do the trick
    m = size(X,1);
    acumJ = 0;
    for i = 1:m
        y_estimated = out(:,i);
        y_labeled = eye_mapping(y(i),:);
        acumJ = acumJ + ( y_labeled * log ( y_estimated ) + ( 1 - y_labeled ) * log ( 1 - y_estimated ) );
    end

    J1 = - ( 1.0 / m ) * acumJ;

    Theta1_nobias = Theta1(:,2:end);
    Theta2_nobias = Theta2(:,2:end);

    J2 = ( lambda / ( 2 * m ) ) * ( sum( ( Theta1_nobias .^ 2 ), [1 2] ) + sum( ( Theta2_nobias .^ 2 ), [1 2] ) );
    J = J1 + J2;

    %
    % Part 2: Implement the backpropagation algorithm to compute the gradients
    %         Theta1_grad and Theta2_grad. You should return the partial derivatives of
    %         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
    %         Theta2_grad, respectively. After implementing Part 2, you can check
    %         that your implementation is correct by running checkNNGradients
    %
    %         Note: The vector y passed into the function is a vector of labels
    %               containing values from 1..K. You need to map this vector into a
    %               binary vector of 1's and 0's to be used with the neural network
    %               cost function.
    %
    %         Hint: We recommend implementing backpropagation using a for-loop
    %               over the training examples if you are implementing it for the
    %               first time.
    %
    % same size as theta, but zeros in it
    DELTA_1 = zeros(size(Theta1));
    DELTA_2 = zeros(size(Theta2));

    for i = 1:m

        % forward computing
        a1 = [1, X(i,:)];
        z2 = Theta1 * a1';
        g2 = sigmoid(z2);

        a2 = [1, g2'];
        z3 = Theta2 * a2';
        a3 = sigmoid(z3);

        % back propagation
        y_labeled = eye_mapping(:,y(i));

        delta3 = a3 - y_labeled;
        DELTA_2 = DELTA_2 + delta3 * a2;

        delta2 = Theta2' * delta3 .* [ 1 ; sigmoidGradient(z2) ];
        delta2 = delta2(2:end);
        DELTA_1 = DELTA_1 + delta2 * a1;

    end

    Theta1_grad = DELTA_1 / m;
    Theta2_grad = DELTA_2 / m;

    % Part 3: Implement regularization with the cost function and gradients.
    %
    %  Hint: You can implement this around the code for
    %     backpropagation. That is, you can compute the gradients for
    %     the regularization separately and then add them to Theta1_grad
    %     and Theta2_grad from Part 2.
    %

    Theta1_grad = Theta1_grad + (lambda/m) * [ zeros(hidden_layer_size, 1) , Theta1_nobias ];
    Theta2_grad = Theta2_grad + (lambda/m) * [ zeros(num_labels, 1) , Theta2_nobias ];

    % return unrolled version
    grad = [Theta1_grad(:) ; Theta2_grad(:)];
end
