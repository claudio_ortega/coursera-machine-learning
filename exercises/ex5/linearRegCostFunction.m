%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
    m = length(y);                  % number of training examples

    e = ( X * theta - y );          % error
    theta_nb = [0 ; theta(2:end)];  % no bias, take the bias out
    
    J = (1/(2*m)) * ( e'* e ) + (lambda/(2*m)) * ( theta_nb' * theta_nb );
    grad = (1/m) * ( X' * e ) + (lambda/m) * theta_nb;
end
