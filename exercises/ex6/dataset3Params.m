function [COut, sigmaOut] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

    Cx = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];
    sigmax = [0.001, 0.003, 0.01, 0.03, 0.1, 0.3, 1, 3];

    Cx = Cx(:);
    sigmax = sigmax(:);
    error = zeros( size(Cx,1),size(sigmax,1) );
    
    for i=1:size(Cx, 1)
        for j=1:size(sigmax, 1)

            C = Cx(i);
            sigma = sigmax(j);

            model = svmTrain(X, y, C, @(x1, x2) gaussianKernel(x1, x2, sigma), 1e-3, 5);

            pred = svmPredict(model, Xval);

            error(i,j) = mean( double( pred ~= yval ) );
        end
    end

    disp("error matrix:")
    disp(error)
    
    [~,indexMinLin] = min(error(:));
    [indexMinRow, indexMinCol] = ind2sub(size(error),indexMinLin);

    COut = Cx(indexMinRow);
    sigmaOut = sigmax(indexMinCol);
    
end