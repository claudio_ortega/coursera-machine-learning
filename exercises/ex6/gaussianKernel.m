%RBFKERNEL returns a radial basis function kernel between x1 and x2
%   sim = gaussianKernel(x1, x2) returns a gaussian kernel between x1 and x2
%   and returns the value in sim

function sim = gaussianKernel(x1, x2, sigma)
    % make x1 and x2 column vectors
    d = x1(:) - x2(:);
    sim = exp ( - ( 1.0 / ( 2 * sigma*sigma ) ) * ( d' * d ) );
    end
