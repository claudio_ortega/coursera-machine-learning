function centroids = computeCentroids(X, idx, K)
%COMPUTECENTROIDS returns the new centroids by computing the means of the 
%data points assigned to each centroid.
%   centroids = COMPUTECENTROIDS(X, idx, K) returns the new centroids by 
%   computing the means of the data points assigned to each centroid. It is
%   given a dataset X where each row is a single data point, a vector
%   idx of centroid assignments (i.e. each entry in range [1..K]) for each
%   example, and K, the number of centroids. You should return a matrix
%   centroids, where each row of centroids is the mean of the data points
%   assigned to it.
%

    % Useful variables
    [m n] = size(X);

    % You need to return the following variables correctly.
    centroids = zeros(K, n);
    centroids_n = zeros(K, 1);

    for i=1:m
        k = idx(i);
        centroids(k,:) = centroids(k,:) + X(i,:);
        centroids_n(k,1) = centroids_n(k,1) + 1;
    end

    centroids = centroids ./ centroids_n;

end

