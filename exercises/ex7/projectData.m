function Z = projectData(X, U, K)
    %PROJECTDATA Computes the reduced data representation when projecting only
    %on to the top k eigenvectors
    %   Z = projectData(X, U, K) computes the projection of
    %   the normalized inputs X into the reduced dimensional space spanned by
    %   the first K columns of U. It returns the projected examples in Z.
    %
    % ====================== YOUR CODE HERE ======================
    % Instructions: Compute the projection of the data using only the top K
    %               eigenvectors in U (first K columns).


    % X is m x n
    [m n] = size(X)

    % Ureduce is n x k
    Ureduce = U(:,1:K);

    % Z should be m x K
    Z = X * Ureduce;

end
