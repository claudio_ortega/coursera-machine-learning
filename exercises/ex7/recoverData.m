function X_rec = recoverData(Z, U, K)
    %RECOVERDATA Recovers an approximation of the original data when using the
    %projected data
    %   X_rec = RECOVERDATA(Z, U, K) recovers an approximation the
    %   original data that has been reduced to K dimensions. It returns the
    %   approximate reconstruction in X_rec.
    %

    % Ureduce is n x k
    Ureduce = U(:,1:K);

    % Z is m x k
    % X_rec should m x n
    X_rec = Z * Ureduce';

end
